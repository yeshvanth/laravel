<?php
/**
 * Created by PhpStorm.
 * User: Web Development
 * Date: 25-06-2020
 * Time: 21:52
 */
?>

<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <!--To make it responsive-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!--Required stylesheets -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-social.css')}}">
    <link rel="stylesheet" href="{{ asset('css/Styles.css') }}">
    <title>Login into YM Labs</title>
</head>
<body>
<header class ="jumbotron">
    <div class ="container">
        <div class ="row">
            <div class ="col-12 col-sm-6">
                <h1>Laravel PHP</h1>
                <p>PHP Internship at Layorz</p>
            </div>
        </div>
    </div>
</header>
<div class="container">
    <div class="row row-content justify-content-center">
         <h3 class="justify-content-start">Enter Product Details:</h3>
         <div class="col-12">
              <form method="post">
                   <div class="form-group row">
                       <label for="" class="col-md-3 col-form-label">Enter the product name:</label>
                       <div class="col-md-6">
                           <input type="text" class="form-control" name="productname" placeholder="Product Name">
                       </div>
                   </div>
                   <div class="form-group row">
                       <label for="" class="col-md-3 col-form-label">Enter Product Description:</label>
                       <div class="col-md-6">
                           <input type="text" class="form-control" name="description" placeholder="Product Description">
                       </div>
                   </div>
                  <div class="form-group row">
                      <label for="" class="col-md-3 col-form-label">Enter Price:</label>
                      <div class="col-md-2">
                          <input type="float" class="form-control" name="price" placeholder="Price">
                      </div>
                  </div>
                  <div class="form-group row">
                      <div class="offset-md-4 col-md-10">
                          <button type="submit" name="logproduct" class="btn btn-primary">Submit</button>
                      </div>
                  </div>
              </form>
         </div>
    </div>
</div>
<!-- jQuery first, then Popper.js, then Bootstrap JS. -->
<script src="jquery.slim.min.js"></script>
<script src="popper.min.js"></script>
<script src="bootstrap.min.js"></script>
</body>
</body>
</html>
